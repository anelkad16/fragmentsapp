package com.example.fragmentsapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

class FirstFragment : Fragment() {

    private var onChangeColorClickListener: (() -> Unit)? = null
    private var onSwapClickListener: (() -> Unit)? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_first, container, false)
        view.findViewById<Button>(R.id.btn_swap)
            .setOnClickListener { onSwapClickListener?.invoke() }
        view.findViewById<Button>(R.id.btn_change_color)
            .setOnClickListener { onChangeColorClickListener?.invoke() }
        return view
    }

    fun setOnChangeColorClickListener(listener: () -> Unit) {
        onChangeColorClickListener = listener
    }

    fun setOnSwapClickListener(listener: () -> Unit) {
        onSwapClickListener = listener
    }
}