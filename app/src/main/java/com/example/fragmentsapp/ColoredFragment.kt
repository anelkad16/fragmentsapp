package com.example.fragmentsapp

import android.graphics.Color
import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment

abstract class ColoredFragment(
    color: Int = Color.WHITE,
    @LayoutRes layout: Int
) : Fragment(layout) {

    var backgroundColor: Int = color
        private set

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        savedInstanceState?.getInt("color")?.let { backgroundColor = it }
        view.setBackgroundColor(backgroundColor)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("color", backgroundColor)
    }

    fun onColorChanged(newColor: Int) {
        if (backgroundColor != newColor) {
            backgroundColor = newColor
            view?.setBackgroundColor(newColor)
        }
    }
}