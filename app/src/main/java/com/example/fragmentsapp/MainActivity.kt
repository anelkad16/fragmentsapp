package com.example.fragmentsapp

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import kotlin.properties.Delegates.notNull
import kotlin.random.Random

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    private var fragment1 by notNull<FirstFragment>()
    private var fragment2 by notNull<ColoredFragment>()
    private var fragment3 by notNull<ColoredFragment>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        fragment1 = supportFragmentManager.find<FirstFragment>(R.id.container_1) ?: return
        fragment2 = supportFragmentManager.find<ColoredFragment>(R.id.container_2) ?: return
        fragment3 = supportFragmentManager.find<ColoredFragment>(R.id.container_3) ?: return

        onChangeColorClick()
        onSwapFragmentsClick()
    }

    private fun onChangeColorClick() {
        fragment1.setOnChangeColorClickListener {
            fragment2.onColorChanged(getRandomColor())
            fragment3.onColorChanged(getRandomColor())
        }
    }

    private fun onSwapFragmentsClick() {
        fragment1.setOnSwapClickListener {
            val existedFragment = supportFragmentManager
                .findFragmentById(R.id.container_2) ?: return@setOnSwapClickListener

        val (newFragment2, newFragment3) = when (existedFragment) {
            is SecondFragment -> {
                recreateFragments()
                fragment3 to fragment2
            }
            is ThirdFragment -> {
                recreateFragments()
                fragment2 to fragment3
            }
            else -> return@setOnSwapClickListener
        }
            supportFragmentManager.beginTransaction()
                .replace(R.id.container_2, newFragment2)
                .replace(R.id.container_3, newFragment3)
                .commit()
        }
    }

    private fun recreateFragments() {
        val secondColor = fragment2.backgroundColor
        val thirdColor = fragment3.backgroundColor
        fragment2 = SecondFragment(secondColor)
        fragment3 = ThirdFragment(thirdColor)
    }

    private fun getRandomColor(): Int = Color.rgb(
        Random.nextInt(256), Random.nextInt(256), Random.nextInt(256)
    )

    private inline fun <reified T : Fragment> FragmentManager.find(id: Int): T? =
        (findFragmentById(id) as? T)
}