package com.example.fragmentsapp

import android.graphics.Color

class SecondFragment(
    color: Int = Color.WHITE
) : ColoredFragment(
    layout = R.layout.fragment_second,
    color = color
)