package com.example.fragmentsapp

import android.graphics.Color

class ThirdFragment(
    color: Int = Color.WHITE
) : ColoredFragment(
    layout = R.layout.fragment_third,
    color = color
)